FROM nginx:stable-alpine

WORKDIR /var/www/html
RUN mkdir demo

COPY ./default.html /var/www/html/
COPY ./index.html /var/www/html/demo/
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 9981
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]